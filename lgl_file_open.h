#pragma once

#include <lgl/lgl_color_list.h>
#include <lgl/lgl_color_dim.h>

class lgl_file_open {
public:
	lgl_file_open(string font_file);

	void get_file();

	bool got_file();
	string get_filename();

protected:
	bool m_got_file;
	string m_filename, m_font_file;

	class submit_button : public lgl_button_color_dim {
	public:
		submit_button(int grid_x, int grid_y, string text, lgl_color color, lgl_text * text_engine);

		void on_left_mouse_press();

		bool is_submitted();

	protected:
		bool m_submitted;
	};
};
