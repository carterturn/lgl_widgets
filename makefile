CPP=g++
LFLAGS=-llgl -lGL -lglfw -lfftw3

INC = lgl_file_open.h
SRC = lgl_file_open.cpp

build: $(SRC) $(INC)
	$(CPP) -c -fPIC $(SRC)
	$(CPP) -shared -o liblgl_widgets.so *.o $(LFLAGS)
lgl_file_open:
	$(CPP) lgl_file_open_test.cpp lgl_file_open.cpp -o lgl_file_open_test $(LFLAGS)
install:
	ls /usr/include/lgl
	[ -d /usr/include/lgl/widgets ] || mkdir /usr/include/lgl/widgets
	cp -f *.h /usr/include/lgl/widgets/
	cp -f liblgl_widgets.so /usr/lib/
	chmod a+r -R /usr/include/lgl/widgets
	chmod a+r /usr/lib/liblgl_widgets.so
uninstall:
	[ -d /usr/include/lgl/widgets ] && rm -f /usr/include/lgl/widgets/*.h
	[ -d /usr/include/lgl/widgets/* ] && rmdir /usr/include/lgl/widgets
	rm -f /usr/lib/liblgl_widgets.so
clean:
	for file in $$(ls *.o); do rm $$file; done
	for file in $$(ls *.so); do rm $$file; done
	for file in $$(ls *.gch); do rm $$file; done
rebuild: clean build
