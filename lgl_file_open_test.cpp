#include <iostream>

#include "lgl_file_open.h"

using std::cout;

int main(int argc, char * argv[]){

	lgl_file_open file_open("monospace.bmp");

	file_open.get_file();

	if(file_open.got_file()){
		cout << file_open.get_filename() << "\n";
	}

	return 0;
}
