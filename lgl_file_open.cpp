#include <algorithm>
#include <chrono>
#include <thread>

#include <dirent.h>
#include <unistd.h>

#include <lgl/lgl_edit_indicator.h>
#include <lgl/lgl_text_display.h>
#include <lgl/lgl_window.h>

#include "lgl_file_open.h"

using std::reverse;
using std::chrono::milliseconds;
using std::this_thread::sleep_for;

class file_list_td : public lgl_text_display {
public:
	file_list_td(int grid_x, int grid_y, int height, int width, lgl_color color, lgl_text * text_engine)
		: lgl_text_display(grid_x, grid_y, height, width, color, text_engine){
		m_selected_file = "";
	}

	void on_left_mouse_press(string item) override{
		m_selected_file = item;
	}

	string get_selected_file(){
		return m_selected_file;
	}

	void reset_selected_file(){
		m_selected_file = "";
	}

protected:
	string m_selected_file;
};

lgl_file_open::lgl_file_open(string font_file) : m_font_file(font_file), m_got_file(false) {}

vector<string> get_files(string directory){
	vector<string> files;

	DIR * active_directory = opendir(directory.c_str());

	dirent * directory_data;
	while((directory_data = readdir(active_directory)) != NULL){
		if(directory_data->d_type == DT_DIR){
			files.push_back("* " + string(directory_data->d_name));
		}
		else{
			files.push_back("  " + string(directory_data->d_name));
		}
	}

	closedir(active_directory);

	reverse(files.begin(), files.end());

	return files;
}

void lgl_file_open::get_file(){
	lgl_window window("Open File...");

	if(window.initialize() != 0){
		return;
	}

	lgl_text text_engine(m_font_file);
	text_engine.initialize();

	submit_button close_button(0, 0, "CANCEL", ERA_4_5, &text_engine);
	submit_button open_button(9, 0, "OPEN", ERA_4_3, &text_engine);

	lgl_edit_indicator file_path_indicator(1, 0, 8, ERA_4_4, &text_engine);

	lgl_elbow left_elbow(0, 1, 4, 3, lgl_elbow::ORIENT_TOP_LEFT, "", ERA_4_3, &text_engine);
	lgl_elbow right_elbow(5, 1, 4, 3, lgl_elbow::ORIENT_TOP_RIGHT, "", ERA_4_3, &text_engine);

	file_list_td file_list(1, 1, 5, 8, ERA_4_3, &text_engine);

	window.add_object(&open_button);
	window.add_object(&close_button);

	window.add_object(&file_path_indicator);

	window.add_object(&left_elbow);
	window.add_object(&right_elbow);

	window.add_object(&file_list);

	window.resize();

	char initial_directory[1024];
	getcwd(initial_directory, 1024);

	string current_directory = string(initial_directory);

	while(window.keep_running() && !open_button.is_submitted() && !close_button.is_submitted()){
		file_list.set_text_data(get_files(current_directory));
		
		window.update();

		if(file_list.get_selected_file() != ""){
			string new_file = file_list.get_selected_file();
			if(new_file[0] == '*'){
				new_file = new_file.substr(2);
				if(new_file.length() == 2 && new_file[0] == '.' && new_file[1] == '.'){
					current_directory = current_directory.erase(current_directory.rfind("/"), string::npos);
				}
				else if(new_file.length() == 1 && new_file[0] == '.') {}
				else{
					current_directory += "/" + new_file;
				}
			}
			else{
				new_file = new_file.substr(2);
				file_path_indicator.set_text(new_file);
			}
			file_list.reset_selected_file();
		}

		m_got_file = open_button.is_submitted();

		sleep_for(milliseconds(10));
	}

	string raw_filename = file_path_indicator.get_text();
	if(raw_filename.find_first_not_of(" ") != string::npos){
		m_filename = raw_filename.substr(raw_filename.find_first_not_of(" "));
	}
	else{
		m_filename = "";
	}

	window.cleanup();

	return;
}

bool lgl_file_open::got_file(){
	return m_got_file;
}

string lgl_file_open::get_filename(){
	return m_filename;
}

lgl_file_open::
submit_button::submit_button(int grid_x, int grid_y, string text, lgl_color color, lgl_text * text_engine)
	: lgl_button_color_dim(grid_x, grid_y, 1, text, color, text_engine), m_submitted(false) {}

void lgl_file_open::
submit_button::on_left_mouse_press(){
	m_submitted = true;
}

bool lgl_file_open::
submit_button::is_submitted(){
	return m_submitted;
}

